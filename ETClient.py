#!/usr/bin/env python
# client.py


import sys
import socket


def main():
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = socket.gethostname()
        client.connect((host, 1030))
        client.send(b"Hi there!")
        client.shutdown(socket.SHUT_RDWR)
        client.close()
    except Exception as msg:
        print(msg)

#########################################################

main()
